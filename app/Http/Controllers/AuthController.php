<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('signup');
    }

    public function kirim(Request $request){
        $firstName = $request->fname;
        $lastName = $request->lname;

        return view('welcome', compact('firstName', 'lastName'));
    }
}
