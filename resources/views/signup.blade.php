<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form - Sign Up SanberBook</title>
</head>
<body>
    
    <h1>Buat Account Baru</h1>

    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
        <table border="0" cellspacing="0" cellpadding="5">
            <tr>
                <td>
                    <label for="fname">First Name:</label>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="text" id="fname" name="fname">
                </td>
            </tr>

            <tr>
                <td>
                    <label for="lname">Last Name:</label>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="text" id="lname" name="lname">
                </td>
            </tr>

            <tr>
                <td>
                    <label for="gender">Gender:</label>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="radio" id="male" name="gender"><label for="male">Male</label>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="radio" id="female" name="gender"><label for="female">Female</label>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="radio" id="other" name="gender"><label for="other">Other</label>
                </td>
            </tr>

            <tr>
                <td>
                    <label for="combobox">Nationality:</label>
                </td>  
            </tr>

            <tr>
                <td>
                    <select name="nationality" id="combobox">
                        <option value="Indonesia">Indonesia</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Australia">Australia</option>
                        <option value="Korea">Korea</option>
                        <option value="Malaysia">Malaysia</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    <label for="language">Language Spoken:</label>
                </td>
            </tr>

            <tr>
                <td>
                <input type="checkbox" id="Indonesia"><label for="Indonesia">Bahasa Indonesia</label>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="checkbox" id="English"><label for="English">English</label>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="checkbox" id="Other"><label for="Other">Other</label>
                </td>
            </tr>

            <tr>
                <td>
                    <label for="bio">Bio:</label>
                </td>
            </tr>

            <tr>
                <td>
                    <textarea name="" id="bio" cols="30" rows="10"></textarea>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="submit" value="Sign Up">
                </td>
            </tr>
        </table>
        
    </form>

</body>
</html>